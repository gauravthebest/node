console.log('Before');

getUser(1, displayUser);

function displayUser(user) {
    console.log('user', user);
    getUserRepo(user.gitHubUsername, displayRepos);
}

function displayRepos(repos) {
    console.log('Repos: ', repos);
    getUserCommits(repos[0], displayCommits);
}

function displayCommits(commits) {
    console.log(commits);
    // Callback Hell
}


function getUser(id, callback) {
    setTimeout(() => {
        console.log('Reading a user form database');
        callback({
            id: id,
            gitHubUsername: 'gauravthebest'
        });
    }, 2000);
}


function getUserRepo(username, callback) {
    let repos = ['repo1', 'repo2', 'repo3'];
    setTimeout(() => {
        console.log('Calling Github APIs');
        callback(repos);
    }, 2000);
}

function getCommits(repo, callback) {
    let commits = ['c1', 'c2', 'c3'];
    setTimeout(() => {
        console.log('Calling Github APIs for commits');
        callback(commits);
    }, 2000);
}