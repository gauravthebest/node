async function displayCommits() {
    try {
        const user = await getUser();
        const repos = await getUserRepo(user);
        const commits = await getCommits(repos[0]);
        console.log(commits);
    } catch(err) {
        console.log(err);
    }
}

displayCommits(); // This returns a Promise of type void, this is just a syntactic sugar for promises.

function getUser(id, callback) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Reading a user form database');
            resolve({
                id: id,
                gitHubUsername: 'gauravthebest'
            });
        }, 2000);
    });
}

function getUserRepo(username) {
    return new Promise((resolve, reject) => {
        let repos = ['repo1', 'repo2', 'repo3'];
        setTimeout(() => {
            console.log('Calling Github APIs ', repos);
            resolve(repos);
        }, 2000);
    });
}

function getCommits(repo) {
    return new Promise((resolve, reject) => {
        let commits = ['c1', 'c2', 'c3'];
        setTimeout(() => {
            console.log('Calling Github APIs for commits');
            resolve(commits);
        }, 2000);
    });
}