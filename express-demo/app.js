const Joi = require('joi');
const morgan = require('morgan');
const config = require('config');
const logger = require('./logger');

const startupDebugger = require('debug')('app:startup');
const dbDebugger = require('debug')('app:db');

const express = require('express');
const app = express();
const port = process.env.POST || 3000;
const courses = [
    {id:1, name: "Course1"},
    {id:2, name: "Course2"},
    {id:3, name: "Course3"}
];

app.use(express.json());

app.use((req, res, next) => {
    console.log('Autenticating...');
    next();
});

app.use(logger);

app.use(express.static('public'));

if(app.get('env') == 'development'){
    app.use(morgan('tiny'));
    startupDebugger('Logging...');
}


app.set('view engine', 'pug');
app.set('views', './views'); //default

// some db operation
dbDebugger("DB Connected");

console.log(config.get('name'));
console.log(config.get('mail.host'));
console.log(config.get('mail.pass'));

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.get('/api/courses/', (req, res) => {
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (course) {
        res.send(course);
        return;
    }
    res.status(404).send('No Course Available with this ID..');
    // res.send(req.query); // used to get query parameters.
});

app.post('/api/nc', (req, res) => {
    if (! req.body.name || req.body.name.length < 3) {
        res.status(400).send('Invalid Name');
        return;
    }
    course = {
        id: courses.length + 1,
        name: req.body.name
    }
    courses.push(course);
    res.send(courses);
    console.log(courses);
});

app.post('/api/nc1', (req, res) => {
    const schema = {
        name: Joi.string().min(3).required()
    };
    const result = Joi.validate(req.body, schema);
    if (result.error) {
        console.log(result);
        res.status(400).send(result.error.details[0].message);
        return;
    }
    course = {
        id: courses.length + 1,
        name: req.body.name
    }
    courses.push(course);
    res.send(courses);
});

app.put('/api/courses/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('No Course Available with this ID..');

    //Object Destructuring
    let { error } = validateCourse(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    course.name = req.body.name;
    res.send(course);
});


app.delete('/api/courses/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));

    if (!course) return res.status(404).send('No Course Available with this ID..');

    const index = courses.indexOf(course);
    courses.splice(index, 1);

    res.send(course);
});


app.get('/pug/', (req, res) => {
    res.render('index', {title: "My Express APP", message: "Hello World!"});
});

app.listen(port, () => {
    console.log(`Listining on port ${port}...`);
});

function validateCourse(course) {
    const schema = {
        name: Joi.string().min(3).required()
    };
    return Joi.validate(course, schema);
}