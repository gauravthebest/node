const express = require('express');
const app = express();
const course_router = require('./routes/courses');
const home_router = require('./routes/home');
const logger = require('./middleware/logger');
const port = process.env.POST || 3000;

app.use(express.json());

app.use('/', home_router);
app.use('/api/courses', course_router);


app.listen(port, () => {
    console.log(`Listining on port ${port}...`);
});

