const express = require('express');
const router = express.Router();

const courses = [
    {id:1, name: "Course1"},
    {id:2, name: "Course2"},
    {id:3, name: "Course3"}
];

router.get('/', (req, res) => {
    res.send(courses);
});

router.get('/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (course) {
        res.send(course);
        return;
    }
    res.status(404).send('No Course Available with this ID..');
    // res.send(req.query); // used to get query parameters.
});

router.post('/', (req, res) => {
    const schema = {
        name: Joi.string().min(3).required()
    };
    const result = Joi.validate(req.body, schema);
    if (result.error) {
        console.log(result);
        res.status(400).send(result.error.details[0].message);
        return;
    }
    course = {
        id: courses.length + 1,
        name: req.body.name
    }
    courses.push(course);
    res.send(courses);
});

router.put('/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('No Course Available with this ID..');

    //Object Destructuring
    let { error } = validateCourse(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    course.name = req.body.name;
    res.send(course);
});


router.delete('/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));

    if (!course) return res.status(404).send('No Course Available with this ID..');

    const index = courses.indexOf(course);
    courses.splice(index, 1);

    res.send(course);
});

function validateCourse(course) {
    const schema = {
        name: Joi.string().min(3).required()
    };
    return Joi.validate(course, schema);
}

module.exports = router;