const mongoose = require('mongoose');

mongoose.set('debug',true)

mongoose.connect('mongodb://localhost/playground')
    .then(() => {
        console.log('Connected to MongoDB..');
    })
    .catch((err) => {
        console.log('Could not connected to MongoDB.. due to ' + err);
    });

const courseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        lowercase: true,
        uppercase: true,
        trim: true
    },
    tags: {
        type: Array,
        validate: {
            isAsync: true,
            validator: function(v, callback) {
                setTimeout(() => {
                    // do some async task...
                    const result = v && v.length > 0;
                    callback(result);
                }, 100);
            },
            message: 'This is a custom error message!'
        }
    },
    price: {
        type: Number,
        required: true,
        get: v => Math.round(v),
        set: (v) => {return Math.round(v)}
    }
});

const Course = mongoose.model('Course', courseSchema);

async function addDoc() {
    const course = new Course({
        name: 'Angular Course',
        author: 'Gaurav Gupta',
        tags: ['frontend'],
        isPublished: true,
        price: 16.4
    });

    try{
        const result = await course.save();
        console.log(result);
    } catch(ex) {
        for(field in ex.errors) {
            console.log(field + ': ' + ex.errors[field].message);
        }
    }
}

async function getDocs() {
    const courses = await Course
        .findById('5afcd4f86e945b40cc5e2d8d')
        .select('price')
    console.log(courses);
}

// addDoc();
getDocs();